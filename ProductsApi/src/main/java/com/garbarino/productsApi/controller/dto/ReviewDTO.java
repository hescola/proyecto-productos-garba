package com.garbarino.productsApi.controller.dto;

import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.garbarino.productsApi.controller.dto.ProductDTO.ProductDTOBuilder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonSerialize
@JsonPropertyOrder
public class ReviewDTO {
    
    private String id;

    private String user;

    private String review;
}