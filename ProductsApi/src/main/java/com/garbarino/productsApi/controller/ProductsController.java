package com.garbarino.productsApi.controller;

import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.garbarino.productsApi.config.converter.Mapper;
import com.garbarino.productsApi.controller.dto.ProductDTO;
import com.garbarino.productsApi.domain.valueObject.Product;
import com.garbarino.productsApi.service.ProductsService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/products")
@Api(value = "Api para consulta de productos y sus reviews")
public class ProductsController {

    @Autowired
    private ProductsService productsService;

    @Autowired
    private Mapper<Object> modelMapper;

    @GetMapping("/{id}")
    @ApiOperation(
            value = "Consulta de información de una acción y su cotización actual a partir de su Ticker (identificador)",
            response = ProductDTO.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Producto no encontrado"),
            @ApiResponse(code = 500, message = "Producto no encontrado"),
            @ApiResponse(code = 503, message = "Operación no disponible en este momento"),
    })
    public Mono<ProductDTO> getProduct(@PathVariable String id) {
        return productsService.getProduct(id).map(toProductDTO());
    }

    private Function<Product, ProductDTO> toProductDTO() {
        return product -> modelMapper.map(product, ProductDTO.class);
    }

}
