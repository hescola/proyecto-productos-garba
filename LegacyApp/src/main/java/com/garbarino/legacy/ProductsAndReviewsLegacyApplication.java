package com.garbarino.legacy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.rest.RepositoryRestMvcAutoConfiguration;

@SpringBootApplication(exclude = RepositoryRestMvcAutoConfiguration.class)
public class ProductsAndReviewsLegacyApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductsAndReviewsLegacyApplication.class, args);
    }
}
