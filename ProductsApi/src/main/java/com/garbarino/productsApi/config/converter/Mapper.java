package com.garbarino.productsApi.config.converter;

import java.util.List;
import java.util.stream.Collectors;

public interface Mapper<T> {

    public <D> D map(T source, Class<D> destinationType);

    public default <D> List<D> map(List<T> source, Class<D> destinationType) {
        return source.stream().map(s -> map(s, destinationType)).collect(Collectors.toList());
    }

}
