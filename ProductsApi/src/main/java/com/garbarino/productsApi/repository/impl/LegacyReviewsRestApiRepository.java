package com.garbarino.productsApi.repository.impl;

import java.time.Duration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.RequestHeadersSpec;
import org.springframework.web.util.UriComponentsBuilder;

import com.garbarino.productsApi.cache.FluxReactiveCache;
import com.garbarino.productsApi.cache.ReactiveCache;
import com.garbarino.productsApi.domain.entity.LegacyReview;
import com.garbarino.productsApi.repository.LegacyReviewsRepository;

import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import reactor.core.publisher.Flux;

@Repository
@Log4j2
// TODO: refactor para reusar codigo de ambos repositorios
public class LegacyReviewsRestApiRepository implements LegacyReviewsRepository {

    private final String endPoint = "/reviews";

    private String basePath;

    private ReactiveCache<String, Flux<LegacyReview>> cache;

    public LegacyReviewsRestApiRepository(@Value("${legacy.reviews.api.basepath}") String basePath,
            @Value("${legacy.reviews.api.cache.timeToLive}") Long timeToLive) {
        this.basePath = basePath;
        cache = new FluxReactiveCache<>(Duration.ofSeconds(timeToLive));
    }

    @Override
    // @Cacheable("reviewsByProductId")
    public Flux<LegacyReview> findByProductId(String id) {
        return cache.getCachedValueOrCompute(id, () -> requestReviews(id));
    }

    @SneakyThrows
    public Flux<LegacyReview> requestReviews(String productId) {
        log.info("Starting request for Reviews of product with id " + productId);
        return createClient(productId).exchange()
            .flatMapMany(response -> response.bodyToFlux(LegacyReview.class));
    }

    protected RequestHeadersSpec<?> createClient(String productId) {
        return WebClient.create()
            .get()
            .uri(UriComponentsBuilder.fromHttpUrl(basePath)
                .path(endPoint)
                .queryParam("product_id", productId)
                .build()
                .toUri());
    }
}
