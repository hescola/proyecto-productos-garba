package com.garbarino.productsApi.cache;

import java.time.Duration;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

import javax.inject.Provider;

import lombok.val;
import lombok.extern.log4j.Log4j2;
import reactor.core.publisher.Flux;

/*
 * 
 * Esta clase se utiliza modo de cache ya que Spring no soporta cache de Reactor, y utilizar Caché
 * en una instancia de Publisher no tiene sentido ya que se procesa al momento de suscribirse o
 * solicitar un valor
 * TODO: crear clase abstracta para reusar codigo ya que hay mucha logica repetida
 */
@Log4j2
public class FluxReactiveCache<K, V extends Flux<?>> implements ReactiveCache<K, V> {

    private Map<K, V> cache = new ConcurrentHashMap<>();

    private Duration timeToLive;

    public FluxReactiveCache(Duration timeToLive) {
        super();
        this.timeToLive = timeToLive;
    }

    public V getCachedValueOrCompute(K key, Provider<? extends V> providerFunction) {
        V cached = cache
            .computeIfAbsent(key, prepareToBeCached(key, providerFunction));
        return cached;
    }

    @SuppressWarnings("unchecked")
    public Function<? super K, ? extends V> prepareToBeCached(K key, Provider<? extends V> providerFunction) {
        return k -> {
            val cacheable = (V) providerFunction.get()
                .cache(timeToLive)
                .doOnError((error) -> {
                    log.debug("Removing value  from Cache key {}  :: error :: {}", key, error);
                    cache.remove(key);
                });
            log.debug("Adding Mono value [{}] to cache key {}", cacheable, k);
            return cacheable;
        };
    }
}
