package com.garbarino.productsApi.service;

import com.garbarino.productsApi.domain.valueObject.Product;

import reactor.core.publisher.Mono;

public interface ProductsService {

    Mono<Product> getProduct(String id);

}
