package com.garbarino.productos.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import java.math.BigDecimal;
import java.util.Collections;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.internal.verification.VerificationModeFactory;

import com.garbarino.productos.extension.MockitoExtension;
import com.garbarino.productsApi.config.converter.impl.LegacyReviewMapper;
import com.garbarino.productsApi.domain.entity.LegacyProduct;
import com.garbarino.productsApi.domain.entity.LegacyReview;
import com.garbarino.productsApi.domain.valueObject.Product;
import com.garbarino.productsApi.domain.valueObject.Review;
import com.garbarino.productsApi.exception.NotAvailableException;
import com.garbarino.productsApi.exception.NotFoundProductException;
import com.garbarino.productsApi.repository.LegacyProductsRepository;
import com.garbarino.productsApi.repository.LegacyReviewsRepository;
import com.garbarino.productsApi.service.ProductsService;
import com.garbarino.productsApi.service.product.impl.DefaultProductsService;
import com.garbarino.productsApi.service.product.impl.logger.ReviewsFailLogger;
import com.google.common.collect.Lists;

import lombok.val;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@ExtendWith(MockitoExtension.class)
public class ProductsServiceTest {

    private ProductsService productsService;

    @Mock
    private LegacyProductsRepository productsRepository;

    @Mock
    private LegacyReviewsRepository reviewsRepository;

    @Mock
    private ReviewsFailLogger failLogger;

    @BeforeEach
    private void setup() {
        productsService = new DefaultProductsService(productsRepository, reviewsRepository, failLogger);
    }

    @Nested
    public class GivenValidProductId {

        private String productId;

        private Product completeExpectedProduct;

        @BeforeEach
        private void setup() {
            // TODO mover a un utils
            LegacyReview expectedReview1 = LegacyReview.builder()
                .review("Review del producto x")
                .id("x")
                .user("abc")
                .build();

            LegacyReview expectedReview2 = LegacyReview.builder()
                .review("Review del producto y")
                .id("y")
                .user("xyz")
                .build();

            val reviews = Lists.newArrayList(expectedReview1, expectedReview2);

            val expectedLegacyProduct = LegacyProduct.builder()
                .name("Nombre")
                .price(new BigDecimal(10))
                .listPrice(new BigDecimal(5))
                .description("descripcion")
                .id("1")
                .build();

            given(productsRepository.findById(expectedLegacyProduct.getId()))
                .willReturn(Mono.just(expectedLegacyProduct));
            given(reviewsRepository.findByProductId(expectedLegacyProduct.getId()))
                .willReturn(Flux.fromIterable(reviews));

            completeExpectedProduct = Product.builder()
                .name(expectedLegacyProduct.getName())
                .price(expectedLegacyProduct.getPrice())
                .listPrice(expectedLegacyProduct.getListPrice())
                .description(expectedLegacyProduct.getDescription())
                .id(expectedLegacyProduct.getId())
                .reviews(new LegacyReviewMapper().map(reviews, Review.class))
                .build();

            productId = expectedLegacyProduct.getId();
        }

        @Nested
        public class WhenRetrieveProduct {

            private Mono<Product> product;

            @BeforeEach
            private void setup() {
                product = productsService.getProduct(productId);
            }

            @Test
            public void thenShouldReturnCorrectProductAndLogError() {
                assertEquals(completeExpectedProduct, product.blockOptional().get());
            }
        }

        @Nested
        public class WhenRetrieveProductAndReviewsRepositoryIsUnavailable {

            private Mono<Product> product;

            private Product expectedProductWithoutReviews;

            @Captor
            private ArgumentCaptor<Throwable> captorloggingException;

            @Captor
            private ArgumentCaptor<String> captorloggingMessage;

            @BeforeEach
            private void setup() {
                expectedProductWithoutReviews = completeExpectedProduct.withReviews(Collections.emptyList());
                given(reviewsRepository.findByProductId(productId))
                    .willReturn(Flux.error(new NotAvailableException()));
                product = productsService.getProduct(productId);
            }

            @Test
            public void thenShouldReturnCorrectProductAndLogFail() {
                assertEquals(expectedProductWithoutReviews, product.blockOptional().get());
                verify(failLogger, VerificationModeFactory.times(1)).log(captorloggingMessage.capture(),
                        captorloggingException.capture());
                assertEquals(NotAvailableException.class, captorloggingException.getValue().getClass());
                assertEquals("Error triying to get the REVIEWS for productId 1", captorloggingMessage.getValue());
            }
        }

        @Nested
        public class WhenRetrieveProductAndProductsRepositoryIsUnavailable {

            private String productId;

            @BeforeEach
            private void setup() {
                productId = "1";
                given(productsRepository.findById(productId))
                    .willReturn(Mono.error(new NotAvailableException()));
                given(reviewsRepository.findByProductId(productId))
                    .willReturn(Flux.error(new NotAvailableException()));
            }

            @Test
            public void thenShouldThrowException() {
                assertThrows(NotAvailableException.class, () -> productsService.getProduct(productId).block());
            }

        }
    }

    @Nested
    public class GivenNotExistingProductId {

        private String productId;

        @BeforeEach
        private void setup() {
            productId = "-1";
            given(productsRepository.findById(productId))
                .willReturn(Mono.error(new NotFoundProductException()));
            given(reviewsRepository.findByProductId(productId))
                .willReturn(Flux.empty());
        }

        @Test
        public void whenRetrieveProductThenShouldThrowNotFound() {
            assertThrows(NotFoundProductException.class, () -> productsService.getProduct(productId).block());

        }

    }

}
