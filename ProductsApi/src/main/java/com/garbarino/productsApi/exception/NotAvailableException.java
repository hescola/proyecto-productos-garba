package com.garbarino.productsApi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.SERVICE_UNAVAILABLE, reason = "Product not found")
public class NotAvailableException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public NotAvailableException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotAvailableException() {}

}
