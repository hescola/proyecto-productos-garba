package com.garbarino.productsApi.controller.dto;

import java.math.BigDecimal;
import java.util.List;

import org.hashids.Hashids;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonPropertyOrder({"id", "name", "description", "price", "list_price", "reviews"})
public class ProductDTO {

    public static Hashids hashids = new Hashids("ProductsSalt");

    /*
     * No estoy seguro si a esto se referían con Hash como id o simplemente era generar el hash
     * Del ID original
     * También podría setear este valor en el convertidor
     */
    @JsonProperty(value = "id")
    public String getHashId() {
        return hashids.encode(Math.abs(hashCode()));
    }

    private String name;

    private String description;

    private BigDecimal price;

    @JsonProperty(value = "list_price")
    private BigDecimal listPrice;

    private List<ReviewDTO> reviews;

}
