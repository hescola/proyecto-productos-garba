package com.garbarino.productsApi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR,
        reason = "Internal error while triying to retrieve the product data")
public class ExternalApiServerError extends Throwable {

    private static final long serialVersionUID = 1L;

    public ExternalApiServerError(String message) {
        super(message);
    }

}
