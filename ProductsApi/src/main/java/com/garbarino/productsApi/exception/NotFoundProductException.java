package com.garbarino.productsApi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Product not found")
public class NotFoundProductException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public NotFoundProductException(String message) {
        super(message);
    }
    
    public NotFoundProductException() {
    }

}
