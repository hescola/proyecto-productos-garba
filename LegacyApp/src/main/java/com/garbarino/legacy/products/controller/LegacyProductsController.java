package com.garbarino.legacy.products.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.garbarino.legacy.products.domain.entity.Product;
import com.garbarino.legacy.products.repository.ProductsRepository;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("/products")
@Log4j2
public class LegacyProductsController {

    @Autowired
    private ProductsRepository productsRepository;

    @GetMapping("/{id}")
    public Product getProduct(@PathVariable String id) {
        // No cree services porque no tenía mucho sentido para una app que
        // viene a representar una api existente
        log.info("GET /products/{}", id);
       // throw new RuntimeException();
         return productsRepository.findById(id).orElseThrow(() -> new ProductNotFoundException());
    }

}
