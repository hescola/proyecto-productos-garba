package com.garbarino.productsApi.domain.valueObject;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor
public class Review {
	private String id;
	private String user;
	private String review;
}