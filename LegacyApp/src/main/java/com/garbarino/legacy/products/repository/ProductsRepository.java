package com.garbarino.legacy.products.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.garbarino.legacy.products.domain.entity.Product;

@Repository
public interface ProductsRepository extends MongoRepository<Product, String> {

}
