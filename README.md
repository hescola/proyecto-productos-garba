# Productos

Garbarino - Evaluación Técnica Hernán Adriel Escola
Products Service

La app consisten en dos aplicaciones rest. 
La primera, a la cual llamaremos LegacyApp, expone las dos rutas GET solicitadas. A pesar de estar en la misma app, podrían separarse en aplicaciones distintas sin problema ya que no hay dependencias entre sisalvo por la clase que ingresa datos DUMMY desde el código. Me pareció más práctico que estuvieran juntas ya que no cambiaba mucho la resolución del ejercicio. Esta app solo tiene test de integración y no generé la capa de Service también a modo práctico.
Utiliza MongoDB la cual proveo a través de Docker para que puedan probar el funcionamiento de las dos aplicaciones en conjunto.
La segunda aplicación (ProductsApi) consume ambos servicios y devuelve el producto con sus reviews. Para esta aplicación decidí aprovechar para aprender algo nuevo y la implementé con Spring Reactive aunque tuve algunas complicaciones con la Cahcé. Para la caché, primero probé de la formá clásica usando @Cacheable de Spring  y ehCache como implementación, pero esto no sirvió de nada porque a pesar de cachear la respuesta, como estas eran Tipos Reactivos (flux, y mono) por el comportamiento de los mismo no tenía sentido cahcear la instancia retornada.
Para solucionarlo implementé una caché usando un ConcurrentHashMap (podría haber usado también ehCache), sumado con el metodo cache(ttl) de los Tipos Reactivos. La caché de estos cachea el resultado de la ejecución del mismo, sea o no error. Por eso mismo agregué mi implementación en conjunto con la caché de los mismos para dar con una solución más óptima. El time to live es configurable desde application.properties.

Ambas aplicaciones se encuentran Testeadas con una cobertura arriba del 90%

## Tecnologías y Herramientas
* Java 8
* Lombok
* Maven
* MongoDB
* Spring Reactive, Spring Boot
* Swagger - Generación de documentación de la API. Swagger-UI disponible para visualizar las especificaciones de la API
* JUnit5
* Mockito - Test
* WireMock - Test
* Jacoco - Reporte de cobertura de tests

## Instrucciones para la ejecución

### Requisitos
Para correr el proyecto completos necesario Docker CE y Docker Compose. El equipo HOST debe tener disponible los puertos 8080 (legacyApp), 8081 (nueva api) y 27017 (mongodb)

# EJECUCION COMPLETA (LegacyApp + ProductsAPI + MongoDB)

Parado en el root del proyecto basta con ejecutar

```
docker-compose up
```
Esto iniciará tres servicios:

* LegacyApp en el puerto 8080 de la pc host: http://localhost:8080/legacy
* ProductsApi en el puerto 8081 de la pc host: http://localhost:8081/
* MongoDB en el puerto 27017

una vez iniada la app se puede acceder a la documentación de la nueva api en http://localhost:8081/swagger-ui.html

# Build y configuración de aplicaciones por separado
# Configuración

## NUEVA API para  Productos
Los parámetros configurables de la aplicación se encuentran en ProductsApi/src/main/resources/application.properties
en este archivo se encuentran las siguientes properties:

### Path base de la Api de Productos Legacy
```
legacy.products.api.basepath=http://localhost:8080/legacy
```
### Path base de la Api de Reviews Legacy
```
legacy.reviews.api.basepath=http://localhost:8080/legacy
```
### TTL de la cache de productos
```
legacy.products.api.cache.timeToLive=300
```
### TTL de la cache de reviews
```
legacy.reviews.api.cache.timeToLive=300
```

## Legacy API
Los parámetros configurables de la aplicación se encuentran en LegacyApp/src/main/resources/application.properties
en este archivo se encuentran las siguientes properties:


### Configuracion de la base de datos mongoDB
```
spring.data.mongodb.host=localhost
spring.data.mongodb.port=27017
spring.data.mongodb.database=legacy
```
### Configurar carga de datos Dummy (solo está activa para test y ejecucion con perfil DOCKER)
```
database.data.dummy.preaload=false

```


# Build de las apps por separado

Primero que nada posicionarse en el root del proyecto. Si se necesita cambiar alguna propertie de la aplicación, hacerlo antes de seguir con este proceso.

###Para generar el JAR de cualquiera de las apps es necesario estar parado en el root de los proyectos de las apps (/LegacyApp o /ProductsApi)

ES NECESARIO TENER LIBRE EL PUERTO 5555 para correr los test, de lo contrario 
```
mvn clean install

ignorando los test correr:
mvn clean install -DskipTests=true 

```

Este comando correrán los test, se generará el reporte de cobertura y se buildeará la aplicación generando el archivo JAR en  /target/


### Ejecutar la aplicación
Con el siguiente comando se ejecutará la aplicación con el puerto default 8081 para ProductsAPI y 8080 para Legacy (también configurable en las properties, pero tener en cuenta que si se modifican los puertos es necesario indicar el nuevo basePath con el puerto correcto en el caso de ProductsAPI si se modifca el puerto de Legacy)

Siempre parado en el root de cada proyecto
```
java -jar target/productsApi-1.0.0.jar

java -jar target/legacy-1.0.0.jar
```
Si se desea ocupar otro puerto, por ejemplo el 9000, ejecutar:

```
java -jar target/productsApi-1.0.0.jar --server.port=9000
```

Si se ejecuta la app Legacy sin Docker va a ser necesario especificar en application.properties la config de la misma y que la misma sea accesible. Por default no generará ninguna data Dummy, y es posible directametne alimentar la app desde los datos de la misma. Tambien es posible ejecutar solo MongoDB desde docker ejecutando desde el root:

```
docker-compose start mongodb-legacy
```

### Test Coverage
Jacoco está configurado sólo en la nueva API

En la Ruta /target/testCoverage/index.html se puede ver el informe de la cobertura de los Test

## Autor

* **Hernán Adriel Escola** - [HernanEscola](https://github.com/HernanEscola)

