package com.garbarino.legacy.reviews.domain.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
@Document
public class Review {
    @Id
    private String id;

    private String user;

    private String review;

    @Indexed
    @JsonIgnore
    private String productId;
}
