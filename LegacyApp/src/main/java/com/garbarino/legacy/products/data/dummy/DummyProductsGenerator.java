package com.garbarino.legacy.products.data.dummy;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.garbarino.legacy.products.domain.entity.Product;

import lombok.val;

public class DummyProductsGenerator{

    private int amuntToCreate;
    
    
    public DummyProductsGenerator(int amuntToCreate) {
        super();
        this.amuntToCreate = amuntToCreate;
    }

    public List<Product> getDummyProductsData() {
        return IntStream.range(0, amuntToCreate)
            .boxed()
            .map(index -> index + 1)
            .map(id -> createDummyProduct(id.toString()))
            .collect(Collectors.toList());
    }

    public Product createDummyProduct(String id) {
        val price = new BigDecimal(id);
        val listPrice = price.add(BigDecimal.ONE);
        val newProduct = Product.builder()
            .id(id)
            .name("dummy name for" + id)
            .description("dummy description for " + id)
            .listPrice(listPrice)
            .price(price)
            .build();
        return newProduct;
    }
    
}