package com.garbarino.productsApi.service.product.impl;

import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.garbarino.productsApi.config.converter.Mapper;
import com.garbarino.productsApi.config.converter.impl.LegacyReviewMapper;
import com.garbarino.productsApi.domain.entity.LegacyProduct;
import com.garbarino.productsApi.domain.entity.LegacyReview;
import com.garbarino.productsApi.domain.valueObject.Product;
import com.garbarino.productsApi.domain.valueObject.Review;
import com.garbarino.productsApi.repository.LegacyProductsRepository;
import com.garbarino.productsApi.repository.LegacyReviewsRepository;
import com.garbarino.productsApi.service.ProductsService;
import com.garbarino.productsApi.service.product.impl.logger.ReviewsFailLogger;

import lombok.val;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

@Service
public class DefaultProductsService implements ProductsService {

    private LegacyProductsRepository productRepository;

    private LegacyReviewsRepository reviewsRepository;

    private Mapper<LegacyReview> legacyReviewMapper = new LegacyReviewMapper();

    private ReviewsFailLogger failLogger;

    @Autowired
    public DefaultProductsService(LegacyProductsRepository productRepository,
            LegacyReviewsRepository reviewsRepository, ReviewsFailLogger failLogger) {
        super();
        this.productRepository = productRepository;
        this.reviewsRepository = reviewsRepository;
        this.failLogger = failLogger;
    }

    @Override
    public Mono<Product> getProduct(String productId) {
        Mono<Product> product = productRepository.findById(productId)
            .zipWith(getReviews(productId))
            .map(productAndReviews -> mergeIntoProduct(productAndReviews));
        return product;
    }

    private Mono<List<Review>> getReviews(String productId) {
        return reviewsRepository.findByProductId(productId)
            .map(item -> legacyReviewMapper.map(item, Review.class))
            .doOnError(logGetReviewsError(productId))
            .onErrorResume(this::emptyReviews)
            .collectList();
    }

    private Consumer<? super Throwable> logGetReviewsError(String productId) {
        return error -> failLogger.log("Error triying to get the REVIEWS for productId " + productId, error);
    }

    private Product mergeIntoProduct(Tuple2<LegacyProduct, List<Review>> productAndReviews) {
        val productDTO = productAndReviews.getT1();
        val reviewsDTO = productAndReviews.getT2();
        return Product.builder()
            .id(productDTO.getId())
            .name(productDTO.getName())
            .description(productDTO.getDescription())
            .price(productDTO.getPrice())
            .listPrice(productDTO.getListPrice())
            .reviews(reviewsDTO)
            .build();
    }

    public Flux<Review> emptyReviews(Throwable ex) {
        return Flux.fromIterable(Collections.emptyList());
    }

}
