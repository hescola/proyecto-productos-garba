package com.garbarino.legacy.reviews.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.garbarino.legacy.reviews.domain.entity.Review;
import com.garbarino.legacy.reviews.repository.ReviewsRepository;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("/reviews")
@Log4j2
public class LegacyReviewsController {

    @Autowired
    private ReviewsRepository reviewsRepository;

    @GetMapping
    public List<Review> getByProductId(@RequestParam(value = "product_id") String productId) {
        // No cree services porque no tenía mucho sentido para una app que
        // viene a representar una api existente
        log.info("GET /reviews?product_id={}", productId);
        return reviewsRepository.findByProductId(productId);
    }

}
