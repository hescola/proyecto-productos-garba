package com.garbarino.productsApi.service.product.impl.logger;

import org.springframework.stereotype.Component;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
public class Log4jReviewsFailLogger implements ReviewsFailLogger {


    @Override
    public void log(String message, Throwable error) {
        log.error(message, error);
    }

}
