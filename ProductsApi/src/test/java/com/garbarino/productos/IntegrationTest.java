package com.garbarino.productos;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.garbarino.productsApi.ProductsApplication;

@ExtendWith(SpringExtension.class)
@AutoConfigureWireMock(port = IntegrationTest.WIREMOCK_PORT)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = ProductsApplication.class)
public class IntegrationTest {

    public final static int WIREMOCK_PORT = 5555;

    @Autowired
    private WebTestClient webTestClient;

    @Test
    public void givenValidProductIdWhenRequestProductThenShouldResponseOK() {
        webTestClient.get()
            .uri("/products/x")
            .accept(MediaType.APPLICATION_JSON_UTF8)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .expectBody()
            .json("{\"name\":\"Nombre\",\"description\":\"Descriptivo sobre el producto\",\"price\":100,\"reviews\":[{\"id\":\"x\",\"user\":\"abc\",\"review\":\"Review sobre el producto\"},{\"id\":\"y\",\"user\":\"xyz\",\"review\":\"Review sobre el producto\"}],\"list_price\":50,\"id\":\"0ZPGG3A\"}\n");
    }

    @Test
    public void givenNotValidProductIdWhenRequestProductThenShouldResponseNotFound() {
        webTestClient.get()
            .uri("/products/-1")
            .accept(MediaType.APPLICATION_JSON_UTF8)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    public void whenRequestProductAndExternalServerResponse500ThenShouldResponse500() {
        webTestClient.get()
            .uri("/products/internalError")
            .accept(MediaType.APPLICATION_JSON_UTF8)
            .exchange()
            .expectStatus()
            .isEqualTo(500);
    }

    @Test
    public void whenRequestProductAndExternalApiReponseUnexpectedErrorThenShouldResponse503() {
        webTestClient.get()
            .uri("/products/serviceNotAvailable")
            .accept(MediaType.APPLICATION_JSON_UTF8)
            .exchange()
            .expectStatus()
            .isEqualTo(503);
    }

}
