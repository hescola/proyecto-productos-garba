package com.garbarino.productsApi.config.converter.impl;

import org.modelmapper.config.Configuration.AccessLevel;
import org.springframework.stereotype.Component;

import com.garbarino.productsApi.config.converter.Mapper;

@Component
public class ModelToDtoMapper implements Mapper<Object> {

    org.modelmapper.ModelMapper mapper = new org.modelmapper.ModelMapper();

    public ModelToDtoMapper() {
        mapper.getConfiguration()
            .setFieldMatchingEnabled(true)
            .setFieldAccessLevel(AccessLevel.PRIVATE);
    }

    @Override
    public <D> D map(Object source, Class<D> destinationType) {
        return mapper.map(source, destinationType);
    }

}
