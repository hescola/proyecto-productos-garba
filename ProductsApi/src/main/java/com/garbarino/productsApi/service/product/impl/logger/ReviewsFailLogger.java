package com.garbarino.productsApi.service.product.impl.logger;

public interface ReviewsFailLogger {

    void log(String message, Throwable error);

}
