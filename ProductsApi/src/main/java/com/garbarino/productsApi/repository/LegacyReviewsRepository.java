package com.garbarino.productsApi.repository;

import com.garbarino.productsApi.domain.entity.LegacyReview;

import reactor.core.publisher.Flux;

public interface LegacyReviewsRepository {

    Flux<LegacyReview> findByProductId(String id);
}