package com.garbarino.productsApi.repository;

import com.garbarino.productsApi.domain.entity.LegacyProduct;

import reactor.core.publisher.Mono;

public interface LegacyProductsRepository {

    Mono<LegacyProduct> findById(String id);
}