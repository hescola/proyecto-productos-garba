package com.garbarino.productsApi.repository.impl;

import java.time.Duration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.RequestHeadersSpec;
import org.springframework.web.util.UriComponentsBuilder;

import com.garbarino.productsApi.cache.MonoReactiveCache;
import com.garbarino.productsApi.cache.ReactiveCache;
import com.garbarino.productsApi.domain.entity.LegacyProduct;
import com.garbarino.productsApi.exception.ExternalApiServerError;
import com.garbarino.productsApi.exception.NotAvailableException;
import com.garbarino.productsApi.exception.NotFoundProductException;
import com.garbarino.productsApi.repository.LegacyProductsRepository;

import lombok.val;
import lombok.extern.log4j.Log4j2;
import reactor.core.publisher.Mono;

@Repository
@Log4j2
public class LegacyProductsRestApiRepository implements LegacyProductsRepository {

    private String productsEndPoint = "/products/{id}";

    private String basePath;

    private ReactiveCache<String, Mono<LegacyProduct>> cache;

    public LegacyProductsRestApiRepository(@Value("${legacy.products.api.basepath}") String basePath,
            @Value("${legacy.products.api.cache.timeToLive}") Long timeToLive) {
        this.basePath = basePath;
        cache = new MonoReactiveCache<>(Duration.ofSeconds(timeToLive));
    }

    public Mono<LegacyProduct> requestProduct(String productId) {
        return createClient(productId)
            .retrieve()
            .onStatus(status -> status.compareTo(HttpStatus.NOT_FOUND) == 0,
                    response -> Mono.error(new NotFoundProductException(response.statusCode().toString())))
            .onStatus(status -> status.compareTo(HttpStatus.INTERNAL_SERVER_ERROR) == 0,
                    response -> Mono.error(new ExternalApiServerError(response.statusCode().toString())))
            .bodyToMono(LegacyProduct.class);
        // podr�a agregar un retry si necesitara que las invocaciones fueran failsafe
    }

    public RequestHeadersSpec<?> createClient(String productId) {
        val uri = UriComponentsBuilder
            .fromHttpUrl(basePath)
            .path(productsEndPoint)
            .buildAndExpand(productId)
            .toUri();
        log.info("Calling web service, URL [" + uri + "]");
        return WebClient.create().get().uri(uri);
    }

    /**
     * La implementación de la caché funciona pero al cachear el Mono o Flux, la invocación a la api
     * se sigue realizando, lo único que está haciendo es cachear la instancia pero cuando se
     * observe o consuma, finalmente v a invocar a al cliente
     * Para solucionar esto implementé de forma media robusta una solución para poder tener caché.
     * Seguramente haya una solución mejor pero para el Ejercicio cumple la función.
     * Si fuese una app bloqueante o hago el bloqueo antes de devolver la instancia, podría
     * cachearlo con la abstraccion de Spring, pero quise dejarla asicrónica
     */
    @Override
    // @Cacheable("productById")
    public Mono<LegacyProduct> findById(String id) {
        return cache.getCachedValueOrCompute(id, () -> requestProduct(id)
            .onErrorMap(this::isNotHandledException,
                    exception -> new NotAvailableException("Service unavailable", exception)));
    }

    private boolean isNotHandledException(Throwable exception) {
        return !(exception instanceof ExternalApiServerError)
                && !(exception instanceof NotFoundProductException);
    }
}
