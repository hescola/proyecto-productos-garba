package com.garbarino.productsApi.cache;

import javax.inject.Provider;

import org.reactivestreams.Publisher;

public interface ReactiveCache<K, V extends Publisher<?>> {

    public V getCachedValueOrCompute(K key, Provider<? extends V> providerFunction);
}
