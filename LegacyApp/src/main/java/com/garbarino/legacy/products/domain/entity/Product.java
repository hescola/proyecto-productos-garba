package com.garbarino.legacy.products.domain.entity;

import java.math.BigDecimal;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Data
@Builder
@EqualsAndHashCode
@JsonSerialize
public class Product {
	@Id
	private String id;
	private String name;
	private String description;
	private BigDecimal price;
	@JsonProperty(value = "list_price")
	private BigDecimal listPrice;
	private int stock;
	private boolean used;

}