package com.garbarino.legacy.reviews.data.dummy;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.garbarino.legacy.reviews.domain.entity.Review;

import lombok.val;

public class DummyReviewsGenerator {

    private int amuntToCreate;

    public DummyReviewsGenerator(int amuntToCreate) {
        super();
        this.amuntToCreate = amuntToCreate;
    }

    public List<Review> getDummyReviewsData() {
        return IntStream.range(0, amuntToCreate)
            .boxed()
            .map(index -> index + 1)
            .flatMap(productId -> createManyReviewsForProduct(productId.toString(), productId).stream())
            .collect(Collectors.toList());
    }

    /**
     * Creo tantas review como el id del producto, es decir el producto id:1 tiene una review, y el
     * producto id:5 tiene cinco
     */
    List<Review> createManyReviewsForProduct(String productId, int amountOfProductsToCreate) {
        return IntStream.range(0, amountOfProductsToCreate)
            .boxed()
            .map(index -> createDummyReviewForProduct(productId, index))
            .collect(Collectors.toList());

    }

    Review createDummyReviewForProduct(String productId, Integer index) {
        val reviewId = productId + "_" + index;
        val newReview = Review.builder()
            .id(reviewId)
            .productId(productId)
            .review("review_ " + reviewId)
            .user("user_" + reviewId)
            .build();

        return newReview;
    }

}
