package com.garbarino.legacy.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.garbarino.legacy.products.data.dummy.DummyProductsGenerator;
import com.garbarino.legacy.products.repository.ProductsRepository;
import com.garbarino.legacy.reviews.data.dummy.DummyReviewsGenerator;
import com.garbarino.legacy.reviews.repository.ReviewsRepository;

import lombok.val;
import lombok.extern.log4j.Log4j2;

@Configuration
@Log4j2
public class MongoDummyDataLoader {

    static final int PRODUCTS_TO_CREATE_AMOUNT = 5;

    Integer nextReviewId = 0;

    @Autowired
    private ProductsRepository productsRespository;

    @Autowired
    private ReviewsRepository reviewsRespository;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Value("${database.data.dummy.preaload}")
    private boolean preloadDummyData;

    private DummyProductsGenerator productsDummyGenerator = new DummyProductsGenerator(PRODUCTS_TO_CREATE_AMOUNT);

    private DummyReviewsGenerator reviewsDummyGenerator = new DummyReviewsGenerator(PRODUCTS_TO_CREATE_AMOUNT);

    @Bean(name = "testDataInitializer")
    CommandLineRunner preLoadDataBase() throws Exception {
        return args ->
        {
            if (preloadDummyData) {
                mongoTemplate.getDb().drop(); // limpio la BD

                val products = productsDummyGenerator.getDummyProductsData();
                products.forEach(product ->
                {
                    val newProduct = productsRespository.insert(product);
                    log.debug("Product Inserted: {}", newProduct);
                });

                val reviews = reviewsDummyGenerator.getDummyReviewsData();
                reviews.forEach(review ->
                {
                    val newReview = reviewsRespository.insert(review);
                    log.debug("Product Inserted: {}", newReview);
                });
            }
        };
    }

}
