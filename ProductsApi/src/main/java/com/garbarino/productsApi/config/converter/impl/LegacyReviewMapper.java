package com.garbarino.productsApi.config.converter.impl;

import org.apache.commons.lang3.NotImplementedException;

import com.garbarino.productsApi.config.converter.Mapper;
import com.garbarino.productsApi.domain.entity.LegacyReview;
import com.garbarino.productsApi.domain.valueObject.Review;

public class LegacyReviewMapper implements Mapper<LegacyReview> {

    @SuppressWarnings("unchecked")
    @Override
    public <D> D map(LegacyReview source, Class<D> destinationType) {
        if (destinationType != Review.class) {
            throw new NotImplementedException("No se encuentra implementada mappeo para " + destinationType);
        }
        return (D) Review.builder().user(source.getUser()).review(source.getReview()).id(source.getId()).build();
    }
}
