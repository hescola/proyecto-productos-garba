package com.garbarino.legacy.reviews.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.garbarino.legacy.reviews.domain.entity.Review;

@Repository
public interface ReviewsRepository extends MongoRepository<Review, String> {

    public List<Review> findByProductId(String productId);
}
