package com.garbarino.productsApi.domain.valueObject;

import java.math.BigDecimal;
import java.util.List;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;

@Value
@Builder
@Wither
public class Product {

    private String id;

    private String name;

    private String description;

    private BigDecimal price;

    private BigDecimal listPrice;

    private List<Review> reviews;

}
