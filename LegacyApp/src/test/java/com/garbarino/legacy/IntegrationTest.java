package com.garbarino.legacy;

import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;

/**
 * Como datos de prueba estoy dejando los datos que carga la clase de config MongoDummyDataConfig
 * Si quisiera cargar otros datos podría redefinir el bean asignándole un nombree insertar lo que
 * desee pero a modo práctico del ejercicio estoy reutilizando los datos de prueba que utilizo en la
 * APP final
 */

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = ProductsAndReviewsLegacyApplication.class)
@AutoConfigureWebTestClient
public class IntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    private MongodExecutable mongodExecutable;

    @AfterClass
    void clean() {
        mongodExecutable.stop();
    }

    @BeforeClass
    void setup() throws Exception {
        String ip = "localhost";
        int port = 27017;

        IMongodConfig mongodConfig = new MongodConfigBuilder().version(Version.Main.PRODUCTION)
            .net(new Net(ip, port, Network.localhostIsIPv6()))
            .build();

        MongodStarter starter = MongodStarter.getDefaultInstance();
        mongodExecutable = starter.prepare(mongodConfig);
        mongodExecutable.start();
    }

    @Test
    public void givenValidProductIdWhenRequesProductsExpectedOkResponse() {
        String response = restTemplate.getForObject("/products/1", String.class);
        assertEquals(
                "{\"id\":\"1\",\"name\":\"dummy name for1\",\"description\":\"dummy description for 1\",\"price\":1,\"stock\":0,\"used\":false,\"list_price\":2}",
                response);
    }

    @Test
    public void givenNotExistingProductWhenRequestProductsExpectedNotFound() {
        ResponseEntity<String> response = restTemplate.getForEntity("/products/100", String.class);
        assertEquals(response.getStatusCodeValue(), 404);
    }

    @Test
    public void givenValidProductIdWhenRequestReviewsExpectedOkResponse() {
        String response = restTemplate.getForObject("/reviews?product_id=2", String.class);
        assertEquals(
                "[{\"id\":\"2_0\",\"user\":\"user_2_0\",\"review\":\"review_ 2_0\"},{\"id\":\"2_1\",\"user\":\"user_2_1\",\"review\":\"review_ 2_1\"}]",
                response);
    }

}
