package com.garbarino.productsApi.domain.entity;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonSerialize
public class LegacyProduct {

    private String id;

    private String name;

    private String description;

    private BigDecimal price;

    @JsonProperty("list_price")
    private BigDecimal listPrice;

    private int stock;

    private boolean used;
}